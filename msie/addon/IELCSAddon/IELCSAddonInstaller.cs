﻿using System.ComponentModel;
using System.Configuration.Install;
using System.Runtime.InteropServices;

namespace IELCSAddon
{
    [RunInstaller(true)]
    public partial class IELCSAddonInstaller : Installer
    {
        public IELCSAddonInstaller()
        {
            InitializeComponent();
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {
            base.Install(stateSaver);
            RegistrationServices regsrv = new RegistrationServices();

            if (!regsrv.RegisterAssembly(this.GetType().Assembly, AssemblyRegistrationFlags.SetCodeBase))
            {
                throw new InstallException(Properties.Resources.ERROR_ComRegFail);
            }
        }

        public override void Uninstall(System.Collections.IDictionary savedState)
        {
            base.Uninstall(savedState);
            RegistrationServices regsrv = new RegistrationServices();

            if (!regsrv.UnregisterAssembly(this.GetType().Assembly))
            {
                throw new InstallException(Properties.Resources.ERROR_ComUnregFail);
            }
        }
    }
}
