
// STARTING POINT: for MSIE
lcsMain(getDefaultSettings());

function injectStyles(stylesText) {
	var style = document.createElement('style');
	document.getElementsByTagName('head')[0].appendChild(style);
	var sheet = style.styleSheet || style.sheet;
	
	var ast_css = CSS(stylesText);
	var rules = [];
	var errors = ast_css.stylesheet.parsingErrors;
	var rule, i;

	if (errors.length === 0) {
		var rules_ast = ast_css.stylesheet.rules;

		for (i = 0; i < rules_ast.length; i++) {
			rule = rules_ast[i];
			
			if (rule.type != 'rule') continue;
			
			var selector = rule.selectors.join(' ');
			var body = '';
			
			for (var j = 0; j < rule.declarations.length; j++) {
				var declaration = rule.declarations[j];
				
				if (declaration.type != 'declaration') continue;
				
				body += declaration.property + ':' + declaration.value + ';';
			}
			
			rules.push({ selector: selector, body: body });
		}
	} else {
		if (console && console.log) console.log('Error in injectStyles: Cannot parse CSS');
	}

	for (i = 0; i < rules.length; i++) {
		rule = rules[i];

		if (sheet.insertRule) {
			sheet.insertRule(rule.selector + ' {' + rule.body + '}', 0);
		} else if (sheet.addRule) {
			sheet.addRule(rule.selector, rule.body, -1);
		}
	}
}
