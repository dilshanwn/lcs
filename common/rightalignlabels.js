
(function () {
	// hotfix to stop labels from right aligning
	//   this if condition should be checking the options
	if (true) return;
	
	if (!document.getElementById('__LU_MASTER_BLOCK')) return;
	
	var selectors = [
		'#cntCASTRWCASEDETAIL0 td>font.normalTextLabel',
		'#cntCASTRWCASEDETAIL1 td>font.normalTextLabel',
		'#cntCASTRWCASEDETAIL2 td>font.normalTextLabel'
	].join(',');
	var elems = document.querySelectorAll(selectors);
	for (var i = 0; i < elems.length; i++) {
		var s = elems[i].parentElement.style;
		s.textAlign = 'right';
	}
}());
