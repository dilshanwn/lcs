
function downloadURI(uri, name) {
	var link = document.createElement("a");
	link.download = name;
	link.href = uri;
	link.click();
}

var fileName = (function() {
	var url = window.location.href;
	return url.substring(url.lastIndexOf('/') + 1);
}());

var xml = document.body.innerText;
downloadURI('data:text/plain,' + xml, fileName);
