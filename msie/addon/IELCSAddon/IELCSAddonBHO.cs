﻿
using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

using Microsoft.Win32;
using mshtml;
using SHDocVw;
using System.Text;
using System.Diagnostics;

namespace IELCSAddon
{
    [ComVisible(true)]
    [Guid("6E7396A5-1CB6-427B-833F-A20C47880928")]
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("Enhancements for LCS")]
    public class IELCSAddonBHO: IObjectWithSite
    {
        private WebBrowser webBrowser;
        public static string INJECTED_STYLES_ID = "IELCSAddonBHO_injection";
        private static string SCRIPT_PREFIX = "IELCSAddon.Scripts.";
        private Assembly thisAssembly = Assembly.GetExecutingAssembly();
        private string errorString = "";

        void webBrowser_DocumentComplete(object pDisp, ref object URL)
        {
            string sURL = URL.ToString();
            string[] CONTENT_TARGETS = new string[]{
				"http://lcs.corpnet.ifsworld.com/",
                "https://support.ifsworld.com/"
            };
            bool targetFound = false;

            for (int i = 0; i < CONTENT_TARGETS.Length; i++)
            {
                if (sURL.IndexOf(CONTENT_TARGETS[i]) == 0)
                {
                    targetFound = true;
                    break;
                }
            }

            if (targetFound)
            {
                var document2 = webBrowser.Document as IHTMLDocument2;
                var document3 = webBrowser.Document as IHTMLDocument3;

                if (document2 == null || document3 == null)
                {
                	return;
                }

                var injectedDiv = document3.getElementById(INJECTED_STYLES_ID);

                if (injectedDiv != null)
                {
                    return;
                }

                var journalCollection = document3.getElementsByName("CASE_LOG");
                
                if (journalCollection == null || journalCollection.length == 0)
                {
                	return;
                }
                
                var journal = journalCollection.item(0, 0) as IHTMLElement;
                
                if (journal == null)
                {
                	return;
                }

                string[] resourceList = new string[] {
                    "lcsmain.css",
                    "polyfills.js",
                    "utilities.js",
                    "linkified.js",
                    "lcsmain.js",
                    "defaults.js",
                    "reworkcss.js",
                    "main.js"
                };
                StringBuilder cssBuilder = new StringBuilder();
                StringBuilder jsBuilder = new StringBuilder();

                for (int i = 0; i < resourceList.Length; i++)
                {
                    string resourceName = resourceList[i];
                    StringBuilder builderToAppend = null;

                    string source;

                    if (!LoadEmbeddedTextResource(resourceName, out source))
                    {
                        errorString = source;
                        goto resource_load_fail;
                    }

                    if (resourceName.EndsWith(".js"))
                    {
                        builderToAppend = jsBuilder;
                    }
                    else if (resourceName.EndsWith(".css"))
                    {
                        builderToAppend = cssBuilder;
                    }

                    builderToAppend.Append(source);
                    builderToAppend.Append("\r\n");
                }

                journal.insertAdjacentHTML("beforeBegin", string.Format(
                    "<div id=\"{0}\"></div>", INJECTED_STYLES_ID));

                string styles = cssBuilder.Replace("\r\n", " ").Replace('\t', ' ').ToString();
                jsBuilder.Append(string.Format("injectStyles('{0}');", styles));
                string jscode = jsBuilder.ToString();
                document2.parentWindow.execScript(jscode);

                // make sure we don't fall into resource_load_fail:
                return;

            resource_load_fail:
                // exit if resource loading has failed.
                document2.parentWindow.alert("LCS+ has failed to load a resource: " + errorString);
            }
        }
        
        private bool LoadEmbeddedTextResource(string resourceName, out string content) {
        	try {
                Stream stream = thisAssembly.GetManifestResourceStream(SCRIPT_PREFIX + resourceName);
	        	StreamReader reader = new StreamReader(stream);
	        	content = reader.ReadToEnd();
        	} catch (Exception e) {
        		content = e.StackTrace;
        		return false;
        	}
        	
        	return true;
        }

        #region IObjectWithSite Members

        int IObjectWithSite.SetSite(object site)
        {
#if DEBUG
            if (!Debugger.IsAttached)
            {
                Debugger.Launch();
            }
#endif

            if (site != null)
            {
                webBrowser = (WebBrowser)site;
                webBrowser.DocumentComplete += new DWebBrowserEvents2_DocumentCompleteEventHandler(webBrowser_DocumentComplete);
            }
            else
            {
                webBrowser.DocumentComplete -= new DWebBrowserEvents2_DocumentCompleteEventHandler(webBrowser_DocumentComplete);
                webBrowser = null;
            }
            return 0;
        }

        int IObjectWithSite.GetSite(ref System.Guid guid, out System.IntPtr ppvSite)
        {
            IntPtr punk = Marshal.GetIUnknownForObject(webBrowser);
            int hr = Marshal.QueryInterface(punk, ref guid, out ppvSite);
            Marshal.Release(punk);
            return hr;
        }

        #endregion

        #region Browser Helper Object Registration

        public static string BHO_NAME = "Enhancements for LCS";
        public static string BHO_KEY_NAME = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects";

        [ComRegisterFunction]
        public static void RegisterBHO(Type type)
        {
            RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(BHO_KEY_NAME, true);
            if (registryKey == null)
                registryKey = Registry.LocalMachine.CreateSubKey(BHO_KEY_NAME);
            string guid = type.GUID.ToString("B");
            RegistryKey ourKey = registryKey.OpenSubKey(guid);
            if (ourKey == null)
                ourKey = registryKey.CreateSubKey(guid);
            ourKey.SetValue("", BHO_NAME, RegistryValueKind.String);
            ourKey.SetValue("NoExplorer", 1, RegistryValueKind.DWord);
            registryKey.Close();
            ourKey.Close();
        }

        [ComUnregisterFunction]
        public static void UnregisterBHO(Type type)
        {
            RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(BHO_KEY_NAME, true);
            string guid = type.GUID.ToString("B");
            if (registryKey != null)
                registryKey.DeleteSubKey(guid, false);
        }

        #endregion
    }
}
