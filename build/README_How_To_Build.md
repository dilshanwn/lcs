You need to have installed following tools in order to build everything in this project:

1. Visual Studio 2010, (with a C# compiler and MSBuild, of course).
2. Pizza.

All build scripts must be run with the working directory as `<project root>\build\`.

# All Targets

Start up Visual Studio Command Prompt and simply run MSBuild.

```
> msbuild
```

# Chrome Intermediate Build

Chrome Intermediate Build copies the files needed to run the unpacked chrome extensions to `<project root>\build\chrome\` folder.

```
> BuildChromeIntermediate.cmd
```

# Chrome Binary Extension

You will have to manually create the Chrome Binary Extension. Use the files in `<project root>\build\chrome\` folder (Run Build All or, at least, Chrome Intermediate Build before that).

The created zip file must be put in `<project root>\dist\` folder, and must be named `lcsplus.zip`.

# Building Internet Explorer Add-on

This target will build the IE add-on, create an installer for it, and copy the installer to `<project root>\dist\` folder.

```
> BuildIE.cmd
```
