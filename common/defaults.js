
/**
 * Contains the default settings of the extension
 */
function getDefaultSettings() {
	return {
		stylize_journal: true,
		open_task_new_win: true,
		right_align_labels: false,
		fold_mail_thread: true
	};
}