
function saveOptions() {
	var v_stylize_journal = document.getElementById('stylize_journal').checked;
	var v_open_task_new_win = document.getElementById('open_task_new_win').checked;
	var v_right_align_labels = document.getElementById('right_align_labels').checked;
	var v_fold_mail_thread = document.getElementById('fold_mail_thread').checked;
	
	chrome.storage.sync.set({
		stylize_journal: v_stylize_journal,
		open_task_new_win: v_open_task_new_win,
		right_align_labels: v_right_align_labels,
		fold_mail_thread: v_fold_mail_thread
	}, function() {
		// changes saved!
	});
}

function restoreOptions() {
	chrome.storage.sync.get(getDefaultSettings(), function(items) {
		var e_stylize_journal = document.getElementById('stylize_journal');
		var e_open_task_new_win = document.getElementById('open_task_new_win');
		var e_right_align_labels = document.getElementById('right_align_labels');
		var e_fold_mail_thread = document.getElementById('fold_mail_thread');
		
		e_stylize_journal.checked = items.stylize_journal;
		e_open_task_new_win.checked = items.open_task_new_win;
		e_right_align_labels.checked = items.right_align_labels;
		e_fold_mail_thread.checked = items.fold_mail_thread;
	});
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.getElementById('save').addEventListener('click', saveOptions);
