
/**
 * What we are actually doing is sort of "parsing" the case/task
 * journal. We are going to annotate "spans" of the journal text
 * as various headers and note bodies. Following token types are
 * used to annotate spans of journal text.
 */
var T_LOG_HEADER = "T_LOG_HEADER";
var T_CASE_NOTE_HEADER = "T_CASE_NOTE_HEADER";
var T_TASK_NOTE_HEADER = "T_TASK_NOTE_HEADER";
var T_CASE_EMAIL_HEADER = "T_CASE_EMAIL_HEADER";
var T_TASK_EMAIL_HEADER = "T_TASK_EMAIL_HEADER";
var T_TEXT = "T_TEXT";
var T_TASK_ID = "T_TASK_ID";
var HEADER_TYPES = [
		T_CASE_NOTE_HEADER,
		T_TASK_NOTE_HEADER,
		T_CASE_EMAIL_HEADER,
		T_TASK_EMAIL_HEADER
	];
var TASK_HEADER_TYPES = [
		T_TASK_EMAIL_HEADER,
		T_TASK_NOTE_HEADER
	];

var MODIFIED_FROM = ' modified from: "';
var MODIFIED_TO = '" to: "';
var SET_TO = ' set to: {1,2}"';

/**
 * Gets the case id of the page.
 */
function getCaseId() {
	var caseId;
	var kvp = document.getElementById('__LU_KEY_VALUE_PAIR').value;
	
	kvp.split('^').forEach(function (element, index, array) {
		if (element.indexOf('CASE_ID=') === 0) {
			caseId = element.substring('CASE_ID='.length);
		}
	});
	
	return caseId;
}

/**
 * Extracts timestamp, timezone, and sender from a line of the journal
 */
function extractSendingDetailsAndTimestamp(line) {
	// e.g.:
	// Sent: 2015-02-18 08:18:56 (GMT+5.5) by XXXXLK
	// Received: 2014-05-24 09:35:52 (GMT+5.5) by XXXXLK
	var timestampRegex = /\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/g;
	var timezoneRegex = /(GMT[\+-][\d\.]{1,3})/g;
	var result = {};
	
	timestampResult = timestampRegex.exec(line);
	timezoneResult = timezoneRegex.exec(line);
	
	if (timestampResult) {
		result.timestamp = timestampResult[0];
	}
	
	if (timezoneResult) {
		result.timezone = timezoneResult[0];
	}
	
	result.sender = line.substring(line.indexOf(') by ') + ') by '.length);
	result.timestampIndex = timestampResult === null ? -1 : timestampResult.index;
	
	return result;
}

/**
 * Creates a mailto link
 */
function createMailto(address) {
	return '<a href="mailto:' + address + '">' + address + '</a>';
}

/**
 * Takes a case/task journal dump as a string, parses it and passes
 * the parsed journal to a callback.
 */
function parseCaseLog(journal, callback) {
	/**
	 * These regular expressions are used to identify portions
	 * that are case/task log headers, or case/task note headers.
	 */
	var regexes = [
		{
			// e.g.: ------------------ Log ------------------
			label: T_LOG_HEADER,
			re: /-{4,} Log -{4,}[\r\n]/g
		},
		{
			// e.g.: ------ Case Note - Customer - shown externally ------
			label: T_CASE_NOTE_HEADER,
			re: /-{4,} Case Note - [A-Za-z]+ - [A-Za-z ]+ -{4,}[\r\n]/g
		},
		{
			// e.g.: ------ Task Note -90- Internal - shown internally ------
			label: T_TASK_NOTE_HEADER,
			re: /-{4,} Task Note -[0-9]+- [A-Za-z]+ - [A-Za-z ]+ -{4,}[\r\n]/g
		},
		{
			// e.g.: ------ Case E-mail - Internal - shown internally ------
			label: T_CASE_EMAIL_HEADER,
			re: /-{4,} Case E-mail - [A-Za-z]+ - [A-Za-z ]+ -{4,}[\r\n]/g
		},
		{
			// e.g.: ------ Task E-mail -100- Internal - shown internally ------
			label: T_TASK_EMAIL_HEADER,
			re: /-{4,} Task E-mail -[0-9]+- [A-Za-z]+ - [A-Za-z ]+ -{4,}[\r\n]/g
		}
	];
	
	var spans = [];
	
	/**
	 * When supplied with the start and the end of a new span, this function
	 * will bisect (or trisect) an existing span to reflect the addition of
	 * new span.
	 *
	 * e.g.
	 * 1. Suppose the journal was annotate like this:
	 *
	 * Text     : The quick brown fox jumps over the lazy dog.
	 * Position : ^-- 0     ^-- 10    ^-- 20    ^-- 30    ^-- 40
	 * Spans    : |<---------------------------------------->|
	 *                T_SENTENCE (start: 0, end: 43)
	 * 
	 * 2. Now, suppose we call _insertSpan(20, 24, T_VERB). The result would be:
	 *
	 * Text     : The quick brown fox jumps over the lazy dog.
	 * Position : ^-- 0     ^-- 10    ^-- 20    ^-- 30    ^-- 40
	 * Spans    : |<---------------->||<->||<--------------->|
	 *                T_SENTENCE        ^      T_SENTENCE
	 *                 (0, 19)          |        (25, 43)
	 *                                T_VERB
	 *                        (start: 20, length: 24)
	 */
	function _insertSpan(spanStart, spanEnd, spanType) {
		// find the existing span that should enclose the new span
		var enclosingSpanIndex = spans.findIndex(function(element, ix, array) {
			return (element.start <= spanStart) && (element.end >= spanEnd);
		});
		var enclosingSpan = spans[enclosingSpanIndex];
		
		// this array holds the new spans that will eventually replace the
		// existing enclosingSpan
		var newSpans = [
			{
				start: spanStart,
				end: spanEnd,
				type: spanType
			}
		];
		
		// if the new span does not immediately start in enclosingSpan,
		// we should create another new span before the new span with the same
		// type as enclosingSpan.
		if (spanStart > enclosingSpan.start) {
			newSpans.unshift({
				start: enclosingSpan.start,
				end: (spanStart - 1),
				type: enclosingSpan.type
			});
		}
		
		// if the new span does not end with the enclosingSpan, we should
		// create another new span after the new span with the same type as
		// enclosingSpan.
		if (spanEnd < enclosingSpan.end) {
			newSpans.push({
				start: spanEnd + 1,
				end: enclosingSpan.end,
				type: enclosingSpan.type
			});
		}
		
		var spliceArgs = [ enclosingSpanIndex, 1 ];
		spliceArgs = Array.prototype.concat.apply(spliceArgs, newSpans);
		Array.prototype.splice.apply(spans, spliceArgs);
	} // end of function _insertSpan.

	// TODO: Get rid of 'null' text at the end of a case journal.
	
	// We are starting by annotating the whole journal as a note body.
	spans.push({
		start: 0,
		end: (journal.length - 1),
		type: T_TEXT
	});
	
	var i;
	
	// tokenize and annotate the case/task journal.
	for (i = 0; i < regexes.length; i++) {
		var re = regexes[i].re;
		var myArray;
		
		while ((myArray = re.exec(journal)) !== null) {
			var token = myArray[0];
			var tokenStart = myArray.index;
			var tokenEnd = tokenStart + token.length - 1;
			_insertSpan(myArray.index, tokenEnd, regexes[i].label);
		}
	}
	
	// We're done annotating spans. Time to parse the entries and extract data.
	var entries = [];
	
	// Spans are coupled as (header + body). So we can process two spans at a time.
	for (i = 0; i < spans.length; i += 2) {
		var headerSpan = spans[i];
		var bodySpan = spans[i + 1];
		var headerText = journal.substring(headerSpan.start, headerSpan.end);
		var bodyTextLines = journal.substring(bodySpan.start, bodySpan.end).split(/\r|\n|\r\n/g);
		var noteText;
		var entry = {};
		
		entry.headerType = headerSpan.type;
		entry.body = [];
		
		if (TASK_HEADER_TYPES.indexOf(entry.headerType) != -1) {
			// Extract Task ID from the header
			entry.taskId = /[1-9][0-9]*/g.exec(headerText)[0];
		}
		
		// Extract meta information from note/email header
		if (HEADER_TYPES.indexOf(entry.headerType) != -1) {
			entry.internal = (headerText.indexOf('- Internal -') != -1);
		}
		
		var line;
		var details;
		
		if (entry.headerType == T_LOG_HEADER) {
			// If this is a log entry, just get the timestamp/timezone/user info and get out.
			line = bodyTextLines[0];
			details = extractSendingDetailsAndTimestamp(line);
			entry.timestamp = details.timestamp;
			entry.timezone = details.timezone;
			entry.sender = details.sender;
			entry.body.push(line.substring(0, details.timestampIndex - 2));
			entry.logAttributes = [];
			
			for (var n = 1; n < bodyTextLines.length; n++) {
				var reModifiedFrom = new RegExp(MODIFIED_FROM, 'g');
				var reModifiedTo = new RegExp(MODIFIED_TO, 'g');
				var reSetTo = new RegExp(SET_TO, 'g');
				
				var detail = bodyTextLines[n];
				var result;
				
				if (result = reSetTo.exec(detail)) {
					var match = result.index;
					var attribute = detail.substring(0, match);
					var setTo = detail.substring(match + result[0].length, detail.length - 1);
					entry.logAttributes.push({
						type: 'SET',
						attribute: attribute,
						value: setTo
					});
				} else if (result = reModifiedFrom.exec(detail)) {
					var match;
					var match2;
					var oldValue;
					var newValue;
					var attribute;
					var result2;
					
					match = result.index;
					attribute = detail.substring(0, match);
					
					if (result2 = reModifiedTo.exec(detail)) {
						match2 = result2.index;
						oldValue = detail.substring(match + result[0].length, match2);
						newValue = detail.substring(match2 + result2[0].length, detail.length - 1);
					}
					
					entry.logAttributes.push({
						type: 'MODIFY',
						attribute: attribute,
						oldValue: oldValue,
						newValue: newValue
					});
				}
			}
		} else {
			// Remove meta information from note/email body and format the note/email body.
			var processingMeta = true;
			var firstLineFound = false;
	
			for (var j = 0; j < bodyTextLines.length; j++) {
				line = bodyTextLines[j];
				var timestampResult;
				var timezoneResult;
				var isLastTextLine = (j == bodyTextLines.length - 2);
				
				if (processingMeta && line.indexOf('From: ') === 0) {
					// 'From: ' is unreliable because it can be *AUTOUPLOAD. Ignore!
					entry.uploader = line.substring('From: '.length);
					entry.autoUpload = (entry.uploader.indexOf('AUTOUPLOAD') != -1);
				} else if (processingMeta && line.indexOf('Created By: ') === 0) {
					// e.g.:
					// Created By: XXXXLK
					entry.uploader = line.substring('Created By: '.length);
				} else if (processingMeta && (line.search(/Sent: |Received: /g) === 0)) {
					// e.g.:
					// Sent: 2015-02-18 08:18:56 (GMT+5.5) by XXXXLK
					// Received: 2014-05-24 09:35:52 (GMT+5.5) by XXXXLK
					details = extractSendingDetailsAndTimestamp(line);
					entry.timestamp = details.timestamp;
					entry.timezone = details.timezone;
					entry.sender = details.sender;
				} else if (processingMeta && line.indexOf('To: ') === 0) {
					// e.g.:
					// To: emp.loyee@ifsworld.com
					entry.toAddress = line.substring(4).split(';');
				} else if (processingMeta && line.indexOf('Cc: ') === 0) {
					// e.g.:
					// Cc: customer.email@example.com
					entry.ccAddress = line.substring(4).split(';');
				} else if (processingMeta && line.indexOf('To :Queue: ') === 0) {
					// e.g.:
					// To :Queue: RCAP Sppt/Distribution
					entry.queuedTo = line.substring('To :Queue: '.length);
				} else if (processingMeta && line.indexOf('To :Assignee: ') === 0) {
					// e.g.:
					// To :Assignee: RCAP Sppt/XXXXLK
					entry.assignedTo = line.substring('To :Assignee: '.length);
				} else if (line === '' && processingMeta) {
					// Ignore the blank line immediately after meta information
					processingMeta = false;
				} else {
					if (!firstLineFound) {
						firstLineFound = true;
						processingMeta = false;
						
						// Get rid of the double quotation (") at the beginning of the first line
						if (line.indexOf('"') === 0) {
							line = line.substring(1);
						}
					}
					
					if (isLastTextLine && line.lastIndexOf('."', line.length - 2) != -1) {
						// Get rid of the dot and double quotation (.") at the end of the last line
						line = line.substring(0, line.length - 2);
					}
					
					entry.body.push(line);
				}
			}
		}
		
		entries.push(entry);
	}
	
	// Time for the callback
	callback(entries);
}

var LABEL_STYLES = {
	'DEFAULT': {
		'data_block': 'data_block',
		'data': 'data',
		'data_left': 'data_left',
		'data_mid': 'data_mid',
		'data_right': 'data_right',
		'data_label': 'data_label'
	},
	'LOG': {
		'data_block': 'data_block',
		'data': 'data_log',
		'data_left': 'data_left',
		'data_mid': 'data_mid',
		'data_right': 'data_right',
		'data_label': 'data_label_log'
	}
};

/**
 * Make a label to be used in a note/email header
 * `items` must be an array of objects having following
 * properties:
 *   1. icon (textual, path to image file)
 *   2. text (textual, label to display)
 *   3. isLabel (boolean)
 * Either icon or text must be defined and non-null.
 */
function makeDataLabel(items, styleName) {
	if (items.length === 0) {
		return;
	}
	
	if (!styleName) {
		styleName = 'DEFAULT';
	}
	
	var style = LABEL_STYLES[styleName];
	var text = '<div class="' + style['data_block'] + '">';
	
	for (var i = 0; i < items.length; i++) {
		var item = items[i];
		
		if ((!item.icon) && (!item.text)) {
			continue;
		}
		
		var classList = style['data'];
		if (i === 0) {
			classList += ' ' + style['data_left'];
		} else if (i == items.length - 1) {
			classList += ' ' + style['data_right'];
		} else {
			classList += ' ' + style['data_mid'];
		}
		
		if (item.isLabel) {
			classList += ' ' + style['data_label'];
		}
		
		if (item.icon) {
			text += '<img src="' + item.icon + '" />';
		}
		
		if (item.text) {
			text += '<span class="' + classList + '">' + item.text + '</span>';
		}
	}
	
	text += '</div>';
	
	return text;
}

/**
 * This function is responsible for making layout changes to the DOM to
 * create the improved case/task journal.
 */
function formatCaseLog(caseLog, entries, options) {
	// Hide the textarea containing the old unformatted case/task journal. Add
	// node '#improved_case_log' immediately after. This new node will contain
	// the formatted case log.
	caseLog.style.display = 'none';
	caseLog.insertAdjacentHTML('afterend', '<div id="improved_case_log" ></div>');
	
	var result = document.getElementById('improved_case_log');
	
	for (var i = 0; i < entries.length; i++) {
		var entry = entries[i];
		var beginHeaderTag;
		var headerText = '';
		var endHeaderTag = '</div>';
		var beginBodyTag = '<div class="log_line log_detail">';
		var noteText = '';
		var endBodyTag = '</div>';
		var taskUrl = '';
		var title = '';
		
		switch (entry.headerType) {
		case T_LOG_HEADER:
			beginHeaderTag = '<div class="log_line log_line_header">';
			title = 'Log';
			break;
		case T_CASE_EMAIL_HEADER:
			beginHeaderTag = '<div class="log_line case_note_header">';
			title = 'Case E-mail';
			break;
		case T_CASE_NOTE_HEADER:
			beginHeaderTag = '<div class="log_line case_note_header">';
			title = 'Case Note';
			break;
		case T_TASK_EMAIL_HEADER:
			beginHeaderTag = '<div class="log_line task_note_header">';
			title = 'Task E-mail';
			break;
		case T_TASK_NOTE_HEADER:
			beginHeaderTag = '<div class="log_line task_note_header">';
			title = 'Task Note';
			break;
		default:
			break;
		}
		
		if (TASK_HEADER_TYPES.indexOf(entry.headerType) != -1) {
			// Replace Task ID with a link
			taskUrl = '<a target="' + (options.open_task_new_win ? '_blank' : '_self') +
					'" href="/login/secured/castrw/TaskDetail.page?CASE_ID=' +
					getCaseId() + '&TASK_ID=' + entry.taskId + '">' + entry.taskId + '</a>';
		}
		
		if (entry.headerType == T_LOG_HEADER) {
			var posted = [];
			posted.push({ text: 'Log', isLabel: true });
			posted.push({ text: 'on ' + entry.timestamp });
			posted.push({ text: entry.timezone });
			posted.push({ text: entry.sender });
			posted.push({ text: entry.body[0] });
			headerText += makeDataLabel(posted, 'LOG');
			
			for (var n = 0; n < entry.logAttributes.length; n++) {
				var attribute = entry.logAttributes[n];
				var labelParams = [];
				var skipLabel = false;
				labelParams.push({ text: attribute.attribute, isLabel: true });
				
				if (attribute.type == 'SET') {
					labelParams.push({ text: attribute.value });
				} else if (attribute.type == 'MODIFY') {
					if (attribute.oldValue) {
						labelParams.push({ text: '<strike>' + attribute.oldValue + '</strike>' });
					}
					
					if (attribute.newValue) {
						labelParams.push({ text: attribute.newValue });
					}
					
					if (!attribute.oldValue && !attribute.newValue) {
						skipLabel = true;
					}
				}
				
				if (!skipLabel) {
					headerText += makeDataLabel(labelParams, 'LOG');
				}
			}
		} else {
			var visibility = (entry.internal ? 'Internal' : 'Customer');
			var visibilityDesc = (entry.internal ? 'shown internally' : 'shown externally');
			
			var mainLabel = [];
			mainLabel.push({ text: title, isLabel: true });
			
			if (TASK_HEADER_TYPES.indexOf(entry.headerType) != -1) {
				mainLabel.push({ text: taskUrl });
			}
			
			mainLabel.push({ text: visibility, isLabel: true });
			mainLabel.push({ text: visibilityDesc });
			
			headerText += makeDataLabel(mainLabel);
			
			var posted = [];
			posted.push({ text: 'Posted', isLabel: true });
			posted.push({ text: 'on ' + entry.timestamp });
			posted.push({ text: entry.timezone });
			posted.push({ text: 'by ' + createMailto(entry.sender) });
			
			if (entry.autoUpload) {
				posted.push({ text: 'via ' + entry.uploader });
			}
			
			headerText += makeDataLabel(posted);
			
			if (entry.toAddress) {
				var toAddressParams = [ { text: 'To', isLabel: true} ];
				entry.toAddress.forEach(function (element, index, array) {
					toAddressParams.push({ text: createMailto(element) });
				});
				headerText += makeDataLabel(toAddressParams);
			}
			
			if (entry.ccAddress) {
				var ccAddressParams = [ { text: 'Cc', isLabel: true} ];
				entry.ccAddress.forEach(function (element, index, array) {
					ccAddressParams.push({ text: createMailto(element) });
				});
				headerText += makeDataLabel(ccAddressParams);
			}
			
			if (entry.queuedTo) {
				var queuedToParams = [];
				queuedToParams.push({ text: 'Queued To', isLabel: true });
				queuedToParams.push({ text: entry.queuedTo });
				headerText += makeDataLabel(queuedToParams);
			}
			
			if (entry.assignedTo) {
				var assignedToParams = [];
				assignedToParams.push({ text: 'Assigned To', isLabel: true });
				assignedToParams.push({ text: entry.assignedTo });
				headerText += makeDataLabel(assignedToParams);
			}
		}
		
		// add the formatted item to the DOM.
		result.insertAdjacentHTML('beforeend', beginHeaderTag + headerText + endHeaderTag);
		
		if (entry.headerType != T_LOG_HEADER) {
			noteText = '<p class="note_body">';
			
			for (var j = 0; j < entry.body.length; j++) {
				var line = entry.body[j];
				line = (new Linkified(line)).toString();
				var isLastTextLine = (j == entry.body.length - 1);
				var prefix;
				
				// Preserve spaces at the beginning of the line
				var leadingSpacesRegex = /^ +/g;
				var leadingSpacesResult = leadingSpacesRegex.exec(line);
				
				if (leadingSpacesResult) {
					line = line.substring(leadingSpacesRegex.lastIndex);
					prefix = leadingSpacesResult[0].replace(/\s/g, function(c) {
						return '&nbsp;';
					});
				} else {
					prefix = '';
				}
				
				// Add line breaks, but not for the last line.
				line += (isLastTextLine ? '' : '<br />');
				noteText += prefix + line;
			}
			
			noteText += '</p>';
			
			if (options.fold_mail_thread) {
				noteText = foldMailThread(noteText);
			}
			
			result.insertAdjacentHTML('beforeend', beginBodyTag + noteText + endBodyTag);
		} // if (entry.headerType != T_LOG_HEADER)
	} // for each entry
}

/**
 * Parse an email and hide the original message
 */
function foldMailThread(text) {
	var foldBeginRe = /-{4,}Original Message-{4,}/g;
	var foldEndRe = /\[Please do not remove this line if you reply to this mail - EMAIL_THREAD_ID\([0-9A-F]{32}\)\]/g;
  
  var beginResult = foldBeginRe.exec(text);
	var endResult = foldEndRe.exec(text);
	
	if (beginResult != null && endResult != null) {
    var beginIndex = beginResult.index;
    var origBeginIndex = foldBeginRe.lastIndex;
		var endIndex = foldEndRe.lastIndex;
		var retText =
			text.substring(0, beginIndex) +
      '<div class="orig_email_wrap">' +
      '<div class="orig_email_header">' +
        '<span class="orig_email_icon">&#x27A5</span>' +
        'Original Message' +
        '<span class="orig_email_click_notice">(Click to toggle)</span>' +
      '</div>' +
			'<div class="orig_email_body">' +
			text.substring(origBeginIndex, endIndex - 1) +
			'</div>' + // end of orig_email_body
			'</div>' + // end of orig_email_wrap
			text.substring(endIndex);
		return retText;
	}
	
	return text;
}

/**
 * Add dynamic behavior after formatting the case log
 */
function addDynamicBehavior(options) {
  addMailFoldBehavior(options);
}

/**
 * Add dynamic behavior on original mail folds
 */
function addMailFoldBehavior(options) {
  var folds = document.getElementsByClassName('orig_email_wrap');
  
  for (var i = 0; i < folds.length; i++) {
    var fold = folds[i];
    var header = fold.childNodes[0];
    var body = fold.childNodes[1];
    
    body.style.display = 'none';
    header.lcsMailFoldBody = body;
    header.addEventListener('click', handleMailFoldToggle, true);
  }
}

/**
 * Handler function for the toggle behavior of mail folds
 */
function handleMailFoldToggle() {
  var oldDisplay = this.lcsMailFoldBody.style.display;
  this.lcsMailFoldBody.style.display = (oldDisplay === 'none') ? 'block' : 'none';
}

/**
 * This function is supposed to execute on page load.
 */
function lcsMain(options) {
	if (!options.stylize_journal) {
		return;
	}
	
	var caseLogs = document.getElementsByName("CASE_LOG");

	if (!caseLogs) {
		return;
	}
	
	var caseLog = caseLogs[0];
	
	if (!caseLog) {
		return;
	}
	
	parseCaseLog(caseLog.innerHTML, function (entries) {
		formatCaseLog(caseLog, entries, options);
		addDynamicBehavior(options);
	});
}
